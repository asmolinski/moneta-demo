import org.javamoney.moneta.Money;
import org.junit.Test;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;
import javax.money.convert.MonetaryConversions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;


public class CurrencyTest {

  @Test
  public void shouldGetCurrencyFromDefaultProvider() {
    CurrencyUnit currency = Monetary.getCurrency("USD");
    assertThat(currency.getCurrencyCode()).isEqualTo("USD");
    Throwable thrown = catchThrowable(() -> Monetary.getCurrency("DUPA"));
    assertThat(thrown).isInstanceOf(UnknownCurrencyException.class);
  }

  @Test
  public void shouldGetCurrencyFromCustomProvider() {
    assertThat(Monetary.getCurrencyProviderNames()).contains("myBitCoinProvider");
    assertThat(Monetary.getCurrency("BTC", "myBitCoinProvider")).isNotNull();
    Throwable thrown = catchThrowable(() -> Monetary.getCurrency("BTC", "default"));
    assertThat(thrown).isInstanceOf(UnknownCurrencyException.class);
  }

  @Test
  public void shouldConvertCurrency() {
    MonetaryAmount euro = Money.of(1, "EUR");
    MonetaryAmount pln = euro.with(MonetaryConversions.getConversion("PLN"));
    assertThat(pln.getNumber().doubleValue()).isBetween(4.0, 5.0);
  }

  @Test
  public void shouldListProviders() {
    System.out.println("Currency providers: " + Monetary.getCurrencyProviderNames());
    System.out.println("Conversion providers: " + MonetaryConversions.getConversionProviderNames());
  }
}
