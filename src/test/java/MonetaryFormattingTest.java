import org.javamoney.moneta.Money;
import org.javamoney.moneta.format.CurrencyStyle;
import org.junit.Test;

import javax.money.MonetaryAmount;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;
import java.util.Locale;

import static org.assertj.core.api.Assertions.assertThat;

public class MonetaryFormattingTest {

  @Test
  public void shouldFormatByLocale() {
    MonetaryAmountFormat usFormat = MonetaryFormats.getAmountFormat(Locale.US);
    MonetaryAmountFormat deFormat = MonetaryFormats.getAmountFormat(Locale.GERMAN);
    MonetaryAmount money = Money.of(10000.50, "USD");
    assertThat(usFormat.format(money)).isEqualTo("USD10,000.50");
    assertThat(deFormat.format(money)).isEqualTo("USD 10.000,50");
  }

  @Test
  public void shouldFormatWithSymbol() {
    MonetaryAmountFormat usFormat = MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.
        of(Locale.US).set(CurrencyStyle.SYMBOL).build());
    MonetaryAmountFormat deFormat = MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.
        of(Locale.GERMAN).set(CurrencyStyle.SYMBOL).build());
    MonetaryAmount money = Money.of(10000.50, "USD");
    assertThat(usFormat.format(money)).isEqualTo("$10,000.50");
    assertThat(deFormat.format(money)).isEqualTo("USD 10.000,50");
  }

  @Test
  public void shouldFormatWithCustomFormat() {
    MonetaryAmountFormat customFormat = MonetaryFormats.getAmountFormat(AmountFormatQueryBuilder.
        of(Locale.US).set(CurrencyStyle.SYMBOL).set("pattern", "00000.000 \u00A4").build());
    MonetaryAmount money = Money.of(0.50, "USD");
    assertThat(customFormat.format(money)).isEqualTo("00000.500 $");
  }
}
