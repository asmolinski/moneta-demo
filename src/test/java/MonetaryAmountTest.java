import org.javamoney.moneta.FastMoney;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.RoundedMoney;
import org.junit.Test;

import javax.money.MonetaryAmount;
import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.javamoney.moneta.function.MonetaryOperators.rounding;

public class MonetaryAmountTest {
  @Test
  public void moneyVsFastMoneyEquality() {
    MonetaryAmount money = Money.of(10, "USD");
    MonetaryAmount fastMoney = FastMoney.of(10, "USD");
    assertThat(money).isNotEqualTo(fastMoney);
    assertThat(money.isEqualTo(fastMoney)).isTrue();
  }

  @Test
  public void moneyVsFastMoneyOverflow() {
    BigDecimal lotsOfMoney = new BigDecimal("1000000000000000000000000000000000");
    MonetaryAmount money = Money.of(lotsOfMoney, "USD");

    assertThat(money.getNumber().numberValue(BigDecimal.class)).isEqualTo(lotsOfMoney);
    Throwable thrown = catchThrowable(() -> FastMoney.of(lotsOfMoney, "USD"));
    assertThat(thrown).isInstanceOf(ArithmeticException.class);

    Throwable thrown1 = catchThrowable(() -> money.getNumber().longValueExact());
    assertThat(thrown1).isInstanceOf(ArithmeticException.class);

    long bullshit = 4089650035136921600L;
    assertThat(money.getNumber().longValue()).isEqualTo(bullshit);
  }

  @Test
  public void moneyComparisons() {
    MonetaryAmount usd10 = Money.of(10, "USD");
    MonetaryAmount usd20 = Money.of(20, "USD");

    assertThat(usd10.isLessThan(usd20)).isTrue();
    assertThat(usd10.isLessThanOrEqualTo(usd20)).isTrue();
    assertThat(usd20.isGreaterThan(usd10)).isTrue();
    assertThat(usd20.isGreaterThanOrEqualTo(usd10)).isTrue();
    assertThat(usd10.isPositive()).isTrue();
    assertThat(usd10.isNegative()).isFalse();
    assertThat(usd10.isZero()).isFalse();
  }

  @Test
  public void moneyArithmetic() {
    MonetaryAmount usd10 = Money.of(10, "USD");
    MonetaryAmount usd20 = Money.of(20, "USD");

    assertThat(usd20.add(usd10)).isEqualTo(Money.of(30, "USD"));
    assertThat(usd20.subtract(usd10)).isEqualTo(Money.of(10, "USD"));
    assertThat(usd10.multiply(10)).isEqualTo(Money.of(100, "USD"));
    assertThat(usd10.divide(2)).isEqualTo(Money.of(5, "USD"));
  }

  @Test
  public void monetaryRounding() {
    MonetaryAmount usd100 = Money.of(100, "USD");
    assertThat(usd100.multiply(0.01234567).with(rounding(HALF_UP, 2))).isEqualTo(Money.of(1.23, "USD"));
    MonetaryAmount roundedUsd10 = RoundedMoney.of(100, "USD", rounding(HALF_UP, 2));
    assertThat(roundedUsd10.multiply(0.01234567).isEqualTo(Money.of(1.23, "USD"))).isTrue();
  }

  @Test
  public void customMonetaryOperator() {
    MonetaryAmount usd100 = Money.of(100, "USD");
    MonetaryAmount squareRoot = usd100.with(hajs -> Money.of(Math.sqrt(hajs.getNumber().doubleValue()), "USD"));
    assertThat(squareRoot).isEqualTo(Money.of(10, "USD"));
  }

}
