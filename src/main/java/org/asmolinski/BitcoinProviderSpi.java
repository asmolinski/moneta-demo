package org.asmolinski;

import org.javamoney.moneta.CurrencyUnitBuilder;

import javax.money.CurrencyQuery;
import javax.money.CurrencyUnit;
import javax.money.spi.CurrencyProviderSpi;
import java.util.Collections;
import java.util.Set;

import static java.util.Collections.singleton;

public class BitcoinProviderSpi implements CurrencyProviderSpi {
  @Override
  public String getProviderName() {
    return "myBitCoinProvider";
  }

  @Override
  public boolean isCurrencyAvailable(CurrencyQuery query) {
    return query.getCurrencyCodes().contains("BTC");
  }

  @Override
  public Set<CurrencyUnit> getCurrencies(CurrencyQuery query) {
    CurrencyUnit bitcoin = CurrencyUnitBuilder.of("BTC", getProviderName()).build();
    return isCurrencyAvailable(query) ? singleton(bitcoin) : Collections.emptySet();
  }

}
